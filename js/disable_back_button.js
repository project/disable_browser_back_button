/**
 * @file
 * JavaScript file for the disable_browser_button module.
 */

(function (Drupal, drupalSettings) {
  'use strict';
  Drupal.behaviors.disable_back_button = {
    attach: function (context, settings) {
      // Array of url's where we have to disable browser back functionality.
      var noback = drupalSettings.noback;
      // Current path of page.
      var pathname = window.location.pathname;
      if ( (noback.indexOf(pathname)) !== '-1') {
        window.history.pushState(null, '', window.location.href);
        window.onpopstate = function () {
          window.history.pushState(null, '', window.location.href);
        };
      }
    }
  };

}(Drupal, drupalSettings));

<?php

namespace Drupal\disable_back_button\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Displays the disable browser back button settings form.
 */
class DisableBackConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'disable_back_button.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'disable_back_button_config_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('disable_back_button.settings');

    // List of url's where we have to disable browser back functionality.
    $form['url_list'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Enter comma seprated page url where you want to disable browser back functionality'),
      '#description' => $this->t('For example you want to disable browser back button on'
           . '/xyz and /abc url then enter (/xyz,/abc) as comma(,) separated'),
      '#default_value' => $config->get('no_back'),
    ];
    // Submit button.
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $url_list = explode(',', $form_state->getValue('url_list'));
    foreach ($url_list as $url) {
      $trim_url = trim($url);
      if ($trim_url[0] !== '/') {
        $form_state->setErrorByName('url_list', $this->t("@url path needs to start with a slash.", ['@url' => $trim_url]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $no_back_string = trim($form_state->getValue('url_list'));
    $trimed_String = str_replace(" ", "", $no_back_string);
    $this->configFactory->getEditable('disable_back_button.settings')
        // Set the submitted configuration setting.
      ->set('no_back', $trimed_String)
      ->save();
  }

}

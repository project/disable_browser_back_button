CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

The disable browser back button module is designed to allow an administrator to 
disable browser back button on specific pages.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/disable_browser_back_button

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/disable_browser_back_button


REQUIREMENTS
------------

No special requirements.


RECOMMENDED MODULES
-------------------

 No recommended modules.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.



CONFIGURATION
-------------

Installation is as simple as copying the module into your 'modules' directory,
then enabling the module.

To enter url's on which administrator wants to disable browser back button go to
 
'Configuration >> System >> Disable Back configuration'
The path for this is /admin/config/browser/noback/settings

For a full description visit project page:
https://www.drupal.org/project/disable_browser_back

Bug reports, feature suggestions and latest developments:
http://drupal.org/project/issues/disable_browser_back


---REQUIREMENTS---

*None. (Other than a clean Drupal 8 installation)
 

MAINTAINERS
-----------
Current maintainers:
* Hardik Patel - https://www.drupal.org/user/3316709/
